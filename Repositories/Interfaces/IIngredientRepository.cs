﻿using RecipeData.Interfaces;
using Repositories;
using System.Collections.Generic;

namespace Interfaces
{
    public interface IIngredientRepository
    {
        IEnumerable<ApiIngredient> GetAll();
        ApiIngredient GetById(int id);
        ApiIngredient Save(IIngredient ingredient);
        ApiIngredient GetEmptyItem();

    }
}