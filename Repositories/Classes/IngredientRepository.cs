﻿using RecipeData.Interfaces;
using RecipeData.Contexts;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using RecipeData.Classes;
using Interfaces;

namespace Repositories
{
    public class IngredientRepository : AbstractRepository<IIngredient>, IIngredientRepository
    {
        public override IEnumerable<ApiIngredient> GetAll()
        {
            using (var db = new RecipeContext())
            {
                var q = from i in db.Ingredients
                            select i;
                return q.ToList<IIngredient>();
            }
        }

        public override ApiIngredient GetById(int id)
        {
            using (var db = new RecipeContext())
            {
                var q = from i in db.Ingredients
                        where i.Id == id
                        select i;
                if (q.Count<IIngredient>() == 0)
                    throw new RowNotInTableException("Ingredient not found");
                else
                    return q.First(); //TODO - Test for multiple results? Shouldn't happen, but....
            }
        }

        public override ApiIngredient GetEmptyItem()
        {
            return new Ingredient();
        }

        public override ApiIngredient Save(IIngredient itemToSave)
        {
            using (var db = new RecipeContext())
            {
                db.Entry(itemToSave).State = itemToSave.Id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
            return itemToSave;
        }

        
    }
}
