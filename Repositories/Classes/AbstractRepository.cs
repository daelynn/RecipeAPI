﻿using RecipeData.Interfaces;
using System.Collections.Generic;

namespace Repositories
{
    public abstract class AbstractRepository<T, I> 
        where T : IRecipeItem
    {
        public abstract IEnumerable<T> GetAll();

        public abstract T GetById(int id);

        public abstract T Save(T itemToSave);

        public abstract T GetEmptyItem();

        public abstract I DataObjectToApiObject(T dataObject);

        public abstract T ApiObjectToDataObject(I apiObject);

    }
}