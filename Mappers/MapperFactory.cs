﻿using Mappers.Implementations;
using RecipeData.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappers
{
    public static class MapperFactory
    {
        private static IngredientMapper ingredientMapper;
        public static IngredientMapper IngredientMapper
        {
            get { return ingredientMapper ?? new IngredientMapper(); }
            set { ingredientMapper = value; }
        }
    }
}
