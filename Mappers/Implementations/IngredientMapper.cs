﻿using RecipeData.Classes;
using RecipeData.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mappers.Implementations
{
    public class IngredientMapper : AbstractMapper<Ingredient, ApiIngredient, IngredientRepository>
    {
        public override Ingredient DataObjectFromApiObject(ApiIngredient sourceItem)
        {
            var targetItem = RepoFactory.IngredientRepo.GetEmptyItem();
            targetItem.Id = sourceItem.Id;
            targetItem.Name = sourceItem.Name;
            targetItem.Description = sourceItem.Description;

            return targetItem;
        }

        public override void Delete(int id)
        {
            RepoFactory.IngredientRepo.Delete(id);
        }

        public override IEnumerable<ApiIngredient> GetAll()
        {
            return RepoFactory.IngredientRepo.GetAll().Select(i => 
                            new ApiIngredient() {
                                Id = i.Id,
                                Name = i.Name,
                                Description = i.Description }).ToList();
        }

        public override ApiIngredient GetById(int id)
        {
            var ing = RepoFactory.IngredientRepo.GetById(id);
            return new ApiIngredient()
            {
                Id = ing.Id,
                Name = ing.Name,
                Description = ing.Description
            };
        }

        public override void Save(ApiIngredient itemToSave)
        {
            Save(-1, itemToSave);
        }

        public override void Save(int id, ApiIngredient itemToSave)
        {
            var ingredient = RepoFactory.IngredientRepo.GetEmptyItem();
            ingredient.Id = itemToSave.Id;
            ingredient.Name = itemToSave.Name;
            ingredient.Description = itemToSave.Description;

            RepoFactory.IngredientRepo.Save(ingredient);
        }



    }
}
