﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RecipeData.Interfaces;

namespace RecipeData.Classes
{
    public class Ingredient
    {
        public string Description { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
