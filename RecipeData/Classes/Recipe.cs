﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeData.Interfaces;
using RecipeData.Utils;

namespace RecipeData.Classes
{
    /// <summary>
    /// Concrete implementation of IRecipe
    /// </summary>
    public class Recipe
    {


        public RecipeTypes Categories { get; set; }

        public string Description { get; set; }

        public ICollection<string> DietaryCompliances { get; private set; }

        public int Id { get; set; }

        public ICollection<byte[]> Images { get; private set; }

        public virtual ICollection<RecipeIngredientGroup> IngredientGroups { get; private set; }

        public ICollection<string> Keywords { get; private set; }

        public string Name { get; set; }

        public string Notes { get; set; }

        public string PrepTime { get; set; }

        public ICollection<string> Instructions { get; private set; }

        public string Yield { get; set; }
    }
}
