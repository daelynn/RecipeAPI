﻿namespace RecipeData.Classes
{
    public class MeasurementUnit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbr { get; set; }
    }
}