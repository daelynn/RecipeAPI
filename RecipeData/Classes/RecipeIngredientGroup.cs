﻿using RecipeData.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeData.Classes
{
    public class RecipeIngredientGroup
    {
        public int Id { get; set; }

        public Recipe Recipe { get; set; }

        public virtual ICollection<Recipe> Ingredients { get; set; }

        public string GroupDescriptor { get; set; }
    }
}
