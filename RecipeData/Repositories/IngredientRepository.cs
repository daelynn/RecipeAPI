﻿using RecipeData.Interfaces;
using RecipeData.Contexts;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using RecipeData.Classes;
using System;

namespace RecipeData.Repositories
{
    public class IngredientRepository : AbstractRepository<Ingredient>, IIngredientRepository
    {
        public override IEnumerable<Ingredient> GetAll()
        {
            using (var db = new RecipeContext())
            {
                var q = from i in db.Ingredients
                            select i;
                return q.ToList();
            }
        }

        public override Ingredient Save(Ingredient itemToSave)
        {
            using (var db = new RecipeContext())
            {
                db.Entry(itemToSave).State = itemToSave.Id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
            return itemToSave;
        }

        public override Ingredient GetById(int id)
        {
            using (var db = new RecipeContext())
            {
                var q = from i in db.Ingredients
                        where i.Id == id
                        select i;
                if (q.Count() == 0)
                    throw new RowNotInTableException("Ingredient not found");
                else
                    return q.First(); //TODO - Test for multiple results? Shouldn't happen, but....
            }
        }

        public override void Delete(int id)
        {
            using (var db = new RecipeContext())
            {
                var itemToDelete = GetEmptyItem();
                itemToDelete.Id = id;
                db.Ingredients.Attach(itemToDelete);
                db.Ingredients.Remove(itemToDelete);
                db.SaveChanges();
            }
        }
    }
}
