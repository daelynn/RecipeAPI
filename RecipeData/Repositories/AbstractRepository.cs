﻿using RecipeData.Contexts;
using System.Collections.Generic;

namespace RecipeData.Repositories
{
    public abstract class AbstractRepository<T> where T : class, new()
    {
        public abstract IEnumerable<T> GetAll();

        public abstract T GetById(int id);

        public abstract T Save(T itemToSave);

        public virtual T GetEmptyItem()
        {
            return new T();
        }

        public virtual void Delete(T itemToDelete)
        {
            using (var db = new RecipeContext())
            {
                db.Entry(itemToDelete).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public abstract void Delete(int id);
    }
}