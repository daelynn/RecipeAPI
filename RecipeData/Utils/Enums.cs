﻿using System;

namespace RecipeData.Utils
{
    [Flags]
    public enum RecipeTypes
    {
        Breakfast = 1,
        Lunch = 1 << 1,
        Dinner = 1 << 2,
        Cocktails = 1 << 3,
        Meat = 1 << 4,
        Poultry = 1 << 5
    }

}