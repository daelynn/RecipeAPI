﻿using RecipeData.Classes;
using System.Collections.Generic;

namespace RecipeData.Interfaces
{
    public interface IIngredientRepository
    {
        IEnumerable<Ingredient> GetAll();
        Ingredient GetById(int id);
        Ingredient Save(Ingredient ingredient);
        Ingredient GetEmptyItem();
        void Delete(Ingredient ingredientToDelete);
        void Delete(int id);
    }
}