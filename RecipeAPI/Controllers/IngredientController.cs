﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using RecipeAPI.Models;
using System.Data;
using RecipeData.Interfaces;
using Mappers;

namespace RecipeAPI.Controllers
{
    public class IngredientController : ApiController
    {
        // GET: api/Ingredient
        public IEnumerable<ApiIngredient> Get()
        {
            return MapperFactory.IngredientMapper.GetAll();
        }

        // GET: api/Ingredient/5
        public ApiIngredient Get(int id)
        {
            try
            {
                return MapperFactory.IngredientMapper.GetById(id);
            }
            catch (RowNotInTableException)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Ingredient
        public void Post([FromBody]ApiIngredient value)
        {
            try
            {
                MapperFactory.IngredientMapper.Save(value);
            }
            catch (Exception e)
            {
                var response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ReasonPhrase = e.Message;
                throw new HttpResponseException(response);
            }
        }

        // PUT: api/Ingredient/5
        public void Put(int id, [FromBody]ApiIngredient value)
        {
            try
            {
                MapperFactory.IngredientMapper.Save(id, value);
            }
            catch (Exception e)
            {
                var response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ReasonPhrase = e.Message;
                throw new HttpResponseException(response);
            }
        }

        // DELETE: api/Ingredient/5
        public void Delete(int id)
        {
            try
            {
                MapperFactory.IngredientMapper.Delete(id);
            }
            catch (Exception e)
            {
                var response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ReasonPhrase = e.Message;
                throw new HttpResponseException(response);
            }
        }

    }
}
