﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RecipeAPI.Controllers;
using Rhino.Mocks;
using System.Data;
using System.Web.Http;
using System.Collections.Generic;
using Mappers.Implementations;
using Mappers;

namespace RecipeAPI.Tests.Controllers
{
    [TestClass]
    public class IngredientControllerTest
    {
        private IngredientController unitUnderTest;
        private IngredientMapper ingredientMapper;

        [TestInitialize]
        public void Setup()
        {
            unitUnderTest = new IngredientController();
            //stub mapper
            ingredientMapper = MockRepository.GenerateStub<IngredientMapper>();
            MapperFactory.IngredientMapper = ingredientMapper;
        }

        #region  Test for GetById
        [TestMethod]
        public void GetByIdReturnsCorrectIngredient()
        {
            //arrange
            var ingredientToFetch = MockRepository.GenerateStub<ApiIngredient>();
            ingredientToFetch.Id = 23;
            ingredientToFetch.Name = "Turmeric";
            ingredientToFetch.Description = "A vibrantly colored spice frequently used in curries.";
            ingredientMapper.Stub(s => s.GetById(23)).Return(ingredientToFetch);

            //act
            var result = unitUnderTest.Get(23);

            //assert
            Assert.AreEqual(result.Id, 23, "IngredientController.get(id) did not return the correct ingredient id!");
            Assert.AreEqual(result.Name, ingredientToFetch.Name, "IngredientController.get(id) did not return the correct ingredient name!");
            Assert.AreEqual(result.Description, ingredientToFetch.Description, "IngredientController.get(id) did not return the correct ingredient Description!");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void GetByIdThrowsHttpNotFoundWhenNotFound()
        {
            //arrange
            ingredientMapper.Stub(s => s.GetById(23)).Throw(new RowNotInTableException());

            //act
            var result = unitUnderTest.Get(23);
        }
        #endregion

        #region Tests for Get

        [TestMethod]
        public void GetReturnsAllIngredients()
        {
            //arrange 
            var turmeric = MockRepository.GenerateStub<ApiIngredient>();
            turmeric.Id = 23;
            turmeric.Name = "Turmeric";
            turmeric.Description = "A vibrantly colored spice frequently used in curries.";

            var coconutMilk = MockRepository.GenerateStub<ApiIngredient>();
            coconutMilk.Id = 42;
            coconutMilk.Name = "Coconut Milk";
            coconutMilk.Description = "One half of the lime and the coconut.";

            var chicken = MockRepository.GenerateStub<ApiIngredient>();
            chicken.Id = 37;
            chicken.Name = "Chicken";
            chicken.Description = "The thing that everything else tastes like.";

            ingredientMapper.Stub(s => s.GetAll()).Return(new List<ApiIngredient> { turmeric, coconutMilk, chicken });

            //act
            //cast result to list for ease of interrogation
            var result = unitUnderTest.Get() as List<ApiIngredient>;

            //assert
            Assert.IsTrue(result.Exists(i => i.Id == turmeric.Id), "Turmeric was not in result set.");
            Assert.IsTrue(result.Exists(i => i.Id == chicken.Id), "Chicken was not in result set.");
            Assert.IsTrue(result.Exists(i => i.Id == coconutMilk.Id), "Coconut milk was not in result set.");
            Assert.IsTrue(result.Count == 3, "Incorrect number of results were returned.");
        }

        [TestMethod]
        public void GetReturnsEmptySetWhenNoIngredientsFound()
        {
            //arrange
            ingredientMapper.Stub(s => s.GetAll()).Return(new List<ApiIngredient>());

            //act
            var result = unitUnderTest.Get() as List<ApiIngredient>;

            //assert
            Assert.IsTrue(result.Count == 0, "Expected empty set but got items.");
        }

        #endregion

        #region Tests for Put

        [TestMethod]
        public void PutCallsSaveWithCorrectValues()
        {
            //arrange
            var valueToPut = new ApiIngredient()
            {
                Id = 5,
                Name = "Jasmine rice",
                Description = "a fragrant white rice, often used in SE Asian dishes."
            };

            //act
            unitUnderTest.Put(valueToPut.Id, valueToPut);

            //assert
            ingredientMapper.AssertWasCalled(x => x.Save(Arg<int>.Matches(arg => arg == valueToPut.Id),
                                            Arg<ApiIngredient>.Matches(arg =>
                                                arg.Id == valueToPut.Id &&
                                                arg.Name == valueToPut.Name &&
                                                arg.Description == valueToPut.Description)));
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void PutThrowsExceptionOnSaveFailure()
        {
            //arrange
            var valueToPut = new ApiIngredient()
            {
                Id = 5,
                Name = "Jasmine rice",
                Description = "a fragrant white rice, often used in SE Asian dishes."
            };

            ingredientMapper.Stub(r => r.Save(0, null)).IgnoreArguments().Throw(new Exception());

            //act
            unitUnderTest.Put(valueToPut.Id, valueToPut);

            //assert.... should never get here!
        }

        #endregion

        #region TestsForPost

        [TestMethod]
        public void PostCallsSaveWithCorrectValues()
        {
            //arrange
            var valueToPost = new ApiIngredient()
            {
                Id = 5,
                Name = "Jasmine rice",
                Description = "a fragrant white rice, often used in SE Asian dishes."
            };

            //act
            unitUnderTest.Post(valueToPost);

            //assert
            ingredientMapper.AssertWasCalled(x => x.Save(Arg<ApiIngredient>.Matches(arg =>
                                                            arg.Id == valueToPost.Id &&
                                                            arg.Name == valueToPost.Name &&
                                                            arg.Description == valueToPost.Description)));
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void PostThrowsExceptionOnSaveFailure()
        {
            //arrange
            var valueToPost = new ApiIngredient()
            {
                Id = 5,
                Name = "Jasmine rice",
                Description = "a fragrant white rice, often used in SE Asian dishes."
            };

            ingredientMapper.Stub(r => r.Save(null)).IgnoreArguments().Throw(new Exception());

            //act
            unitUnderTest.Post(valueToPost);

            //assert.... should never get here!
        }


        #endregion

        #region Tests for Delete

        [TestMethod]
        public void DeleteCallsMapperDeleteWithCorrectValue()
        {
            //arrange
            var delId = 5;
            ingredientMapper.Stub(m => m.Delete(5));

            //act
            unitUnderTest.Delete(delId);

            //assert
            ingredientMapper.AssertWasCalled(m => m.Delete(Arg<int>.Matches(arg => arg == delId)));
            
        }

        #endregion
    }

}
